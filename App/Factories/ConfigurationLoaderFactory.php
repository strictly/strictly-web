<?php

namespace App\Factories;

use Strictly\Configuration\Loader;
use Strictly\Container\ResolverInterface;
use Strictly\Foundation\Contracts\Factory;

class ConfigurationLoaderFactory implements Factory
{
    /**
     * Configuration loaders.
     *
     * @var array $loaders
     */
    private $loaders = [
        \Strictly\Configuration\DotEnv\DotEnvLoader::class,
        \Strictly\Configuration\FileLoader::class,
    ];

    /**
     * @var ResolverInterface
     */
    private $container;

    /**
     * ConfigurationLoaderFactory constructor.
     * @param ResolverInterface $container
     */
    public function __construct(ResolverInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return Loader
     */
    function make() {
        $batchLoader = new \Strictly\Configuration\BatchLoader();
        foreach($this->loaders as $loaderReference) {
            $batchLoader->addLoader(
                $this->container->resolve($loaderReference)
            );
        }

        return $batchLoader;
    }
}