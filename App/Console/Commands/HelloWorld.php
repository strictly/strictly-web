<?php

namespace App\Console\Commands;

use Strictly\Console\ConsoleCommand;

class HelloWorld implements ConsoleCommand
{
    /**
     * Execute the command.
     */
    function execute()
    {
        print "Hello World.".PHP_EOL;
    }

    /**
     * @return string
     */
    function getName(): string
    {
        return "hello-world";
    }

    /**
     * @return string
     */
    function getDescription(): string
    {
        return "Print Hello World.";
    }
}