<?php

namespace App\ServiceProviders;

use Strictly\Container\Container;
use Strictly\Foundation\Contracts\ServiceProvider;
use Strictly\Log\FileWriter;
use Strictly\Log\Writer;

class LogServiceProvider implements ServiceProvider
{
    /**
     * @var Container
     */
    private $container;

    /**
     * LogServiceProvider constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Register Bindings.
     */
    function register()
    {
        $this->container->setReference(Writer::class, FileWriter::class, true);
    }

    /**
     * Configure system.
     */
    function configure()
    {

    }
}