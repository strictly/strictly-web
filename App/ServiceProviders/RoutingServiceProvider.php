<?php

namespace App\ServiceProviders;

use Strictly\Container\Container;
use Strictly\Foundation\Contracts\ServiceProvider;
use Strictly\Http\Routing\Manager;
use Strictly\Http\Routing\ManagerInterface;
use Strictly\Http\Routing\Route\RepositoryFactory;
use Strictly\Http\Routing\Route\Decorators\RouteGroupModificationRepositoryDecorator;
use Strictly\Http\Routing\Route\Repository;
use Strictly\Http\Routing\RouteGroup\ArrayRepository;
use Strictly\Http\Routing\RouteGroup\RouteGroupProcessor;
use Strictly\Http\Routing\RouteGroup\SimpleRepositoryFactory;
use Strictly\Http\Routing\RouteResolverInterface;
use Strictly\Http\Routing\RouteRouteResolver;

class RoutingServiceProvider implements ServiceProvider
{
    /**
     * @var Container
     */
    private $container;

    /**
     * RoutingServiceProvider constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Register Bindings.
     */
    function register()
    {
        $this->container->setReference(ManagerInterface::class, Manager::class);
        $this->container->setReference(\Strictly\Http\Routing\RouteGroup\Repository::class, ArrayRepository::class);
        $this->container->setFactory(Repository::class, RepositoryFactory::class, true);
        $this->container->setReference(ManagerInterface::class, Manager::class);
        $this->container->setReference(RouteResolverInterface::class, RouteRouteResolver::class);
    }

    /**
     * Configure system.
     */
    function configure()
    {
        /** @var \Strictly\Filesystem\Repository $paths */
        $paths = $this->container->resolve(\Strictly\Filesystem\Repository::class);

        /** @var Repository $routeRepository */
        $routeRepository = $this->container->resolve(Repository::class);

        /** @var SimpleRepositoryFactory $routeGroupRepositoryFactory */
        $routeGroupRepositoryFactory = $this->container->resolve(SimpleRepositoryFactory::class);

        $routeGroupRepository = $routeGroupRepositoryFactory->makeRepository();

        $router = new Manager(
            $routeRepository,
            $routeGroupRepository
        );

        require_once $paths->getRoutesPath().DIRECTORY_SEPARATOR.'routes.php';

        /** @var RouteGroupProcessor $processor */
        $processor = $this->container->resolve(RouteGroupProcessor::class);

        $processor->registerRouteGroups($routeGroupRepository->all(), $routeRepository);
    }
}