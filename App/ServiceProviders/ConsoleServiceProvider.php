<?php

namespace App\ServiceProviders;

use Strictly\Console\Commands\ArrayRepository;
use Strictly\Console\Commands\CommandLoader;
use Strictly\Container\Container;
use Strictly\Foundation\Contracts\ServiceProvider;

class ConsoleServiceProvider implements ServiceProvider
{
    /**
     * @var Container
     */
    private $container;

    /**
     * CommandServiceProvider constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Register Bindings.
     */
    function register()
    {
        $this->container->setReference(\Strictly\Console\Commands\Repository::class, ArrayRepository::class, true);
        $this->container->setReference(\Strictly\Console\ExceptionHandler::class, \App\ExceptionHandlers\ConsoleExceptionHandler::class);
    }

    /**
     * Configure post-boot configurations.
     */
    function configure()
    {
        /** @var CommandLoader $commandLoader */
        $commandLoader = $this->container->resolve(CommandLoader::class);
        $commandLoader->execute();
    }
}