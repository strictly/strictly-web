<?php

namespace App\ServiceProviders;

use Strictly\Container\Container;
use Strictly\Foundation\Contracts\ServiceProvider;
use Strictly\Http\ExceptionHandler;
use Strictly\Http\Request\RequestInterface;
use Strictly\Http\Request\SymfonyAdapterFactory;
use Strictly\Http\Response\ResponseFactory;
use App\ExceptionHandlers\Http\AcceptHeaderBasedFactory;
use App\ExceptionHandlers\HttpExceptionHandler;

class WebServiceProvider implements ServiceProvider
{
    /**
     * @var Container
     */
    private $container;

    /**
     * WebServiceProvider constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Register Bindings.
     */
    function register()
    {
        $this->container->setReference(ResponseFactory::class, \Strictly\Http\Response\SymfonyAdapterFactory::class);
        $this->container->setReference(ExceptionHandler::class, HttpExceptionHandler::class);
        $this->container->setFactory(RequestInterface::class, SymfonyAdapterFactory::class, true);
        $this->container->setFactory(HttpExceptionHandler::class, AcceptHeaderBasedFactory::class);
    }

    /**
     * Configure system.
     */
    function configure()
    {

    }
}