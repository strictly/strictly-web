<?php

namespace App\ExceptionHandlers;

use Strictly\Console\CommandException;
use Strictly\Console\Commands\CommandNotFoundException;
use Strictly\Console\Commands\ListCommands;
use Strictly\Console\ConsoleCommand;
use Strictly\Container\ResolverInterface;
use Throwable;

class ConsoleExceptionHandler implements \Strictly\Console\ExceptionHandler
{
    /**
     * @var ResolverInterface
     */
    private $container;

    /**
     * ExceptionHandler constructor.
     * @param ResolverInterface $container
     */
    public function __construct(ResolverInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param CommandException $exception
     */
    function handleCommandException(CommandException $exception)
    {
        print "Error executing command '{$exception->getCommand()->getName()}: {$exception->getMessage()}".PHP_EOL;
    }

    /**
     * Handle command not found.
     *
     * @param CommandNotFoundException $exception
     */
    function handleCommandNotFoundException(CommandNotFoundException $exception)
    {
        if($commandName = $exception->commandName()) {
            print "Command '{$exception->commandName()}' not found.".PHP_EOL;
        }


        $this->listCommands();
    }

    /**
     * Handle an exception.
     *
     * @param Throwable $throwable
     */
    function handle(Throwable $throwable)
    {
        if($throwable instanceof CommandNotFoundException) {
            $this->handleCommandNotFoundException($throwable);
        }

        if($throwable instanceof CommandException) {
            $this->handleCommandException($throwable);
        }

        print "Something bad happened: {$throwable->getMessage()}".PHP_EOL;
    }

    /**
     * List commands.
     */
    private function listCommands() {
        /** @var ConsoleCommand $consoleCommand */
        $consoleCommand = $this->container->resolve(ListCommands::class);
        $consoleCommand->execute();
    }
}