<?php

namespace App\ExceptionHandlers;

use Strictly\Http\ExceptionHandler;
use Strictly\Http\Exceptions\HttpAuthenticationException;
use Strictly\Http\Exceptions\HttpNotFoundException;
use Strictly\Http\Response\ResponseFactory;
use Strictly\Http\Response\ResponseInterface;
use Strictly\Log\Writer;
use Throwable;

abstract class HttpExceptionHandler implements ExceptionHandler
{
    /**
     * @var ResponseFactory
     */
    protected $responseFactory;

    /**
     * @var Writer
     */
    protected $writer;

    /**
     * HttpExceptionHandler constructor.
     * @param ResponseFactory $responseFactory
     */
    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param Throwable $throwable
     * @return ResponseInterface
     */
    function handle(Throwable $throwable): ResponseInterface
    {
        if($throwable instanceof HttpNotFoundException) {
            return $this->handleHttpNotFoundException($throwable);
        }

        if($throwable instanceof HttpAuthenticationException) {
            return $this->handleHttpAuthenticationException($throwable);
        }

        return $this->handleFallback($throwable);
    }

    /**
     * @param Throwable $throwable
     * @return ResponseInterface
     */
    abstract protected function handleFallback(Throwable $throwable): ResponseInterface;

    /**
     * @param \Strictly\Http\Exceptions\HttpNotFoundException $exception
     * @return ResponseInterface
     */
    abstract protected function handleHttpNotFoundException(HttpNotFoundException $exception): ResponseInterface;

    /**
     * @param HttpAuthenticationException $exception
     * @return ResponseInterface
     */
    abstract protected function handleHttpAuthenticationException(HttpAuthenticationException $exception): ResponseInterface;
}