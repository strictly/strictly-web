<?php

namespace App\ExceptionHandlers\Http;

use Strictly\Container\Container;
use Strictly\Foundation\Contracts\Factory;
use Strictly\Http\Request\RequestInterface;
use App\ExceptionHandlers\HttpExceptionHandler;

class AcceptHeaderBasedFactory implements Factory
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var Container
     */
    private $container;

    /**
     * AcceptHeaderBasedFactory constructor.
     * @param RequestInterface $request
     * @param Container $container
     */
    public function __construct(RequestInterface $request, Container $container)
    {
        $this->request = $request;
        $this->container = $container;
    }

    /**
     * @return HttpExceptionHandler
     */
    function make(): HttpExceptionHandler {
        switch ($this->request->headers()->get('Accept')) {
            case 'application/json':
            case 'application+json':
                return $this->container->resolve(JsonHandler::class);
            default:
                return $this->container->resolve(HtmlHandler::class);
        }
    }
}