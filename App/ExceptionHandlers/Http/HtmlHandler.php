<?php

namespace App\ExceptionHandlers\Http;

use App\ExceptionHandlers\HttpExceptionHandler;
use Strictly\Http\Exceptions\HttpAuthenticationException;
use Strictly\Http\Exceptions\HttpNotFoundException;
use Strictly\Http\Response\ResponseInterface;
use Throwable;

class HtmlHandler extends HttpExceptionHandler
{
    protected function handleFallback(Throwable $throwable): ResponseInterface {
        return $this->responseFactory->make('Something REALLY went wrong.', $throwable->getCode() ?: 500);
    }

    /**
     * @param \Strictly\Http\Exceptions\HttpNotFoundException $exception
     * @return ResponseInterface
     */
    protected function handleHttpNotFoundException(HttpNotFoundException $exception): ResponseInterface
    {
        return $this->responseFactory->make('Not found.', $exception->getCode());
    }

    /**
     * @param HttpAuthenticationException $exception
     * @return ResponseInterface
     */
    protected function handleHttpAuthenticationException(HttpAuthenticationException $exception): ResponseInterface {
        return $this->responseFactory->make('Unauthenticated.', $exception->getCode());
    }
}