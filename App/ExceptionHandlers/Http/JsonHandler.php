<?php

namespace App\ExceptionHandlers\Http;

use Strictly\Http\Exceptions\HttpAuthenticationException;
use Strictly\Http\Exceptions\HttpNotFoundException;
use Strictly\Http\Response\ResponseInterface;
use Throwable;
use App\ExceptionHandlers\HttpExceptionHandler;

class JsonHandler extends HttpExceptionHandler
{
    /**
     * @param Throwable $throwable
     * @return ResponseInterface
     */
    protected function handleFallback(Throwable $throwable): ResponseInterface
    {
        return $this->responseFactory->makeJson($this->formatResponseData('Fatal error.'), $throwable->getCode() ?: 500);
    }

    /**
     * @param \Strictly\Http\Exceptions\HttpNotFoundException $exception
     * @return ResponseInterface
     */
    protected function handleHttpNotFoundException(HttpNotFoundException $exception): ResponseInterface
    {
        return $this->responseFactory->makeJson($this->formatResponseData('Not Found.'), $exception->getCode());
    }

    /**
     * @param HttpAuthenticationException $exception
     * @return ResponseInterface
     */
    protected function handleHttpAuthenticationException(HttpAuthenticationException $exception): ResponseInterface
    {
        return $this->responseFactory->makeJson($this->formatResponseData('Unauthenticated.'), $exception->getCode());
    }

    /**
     * @param string $message
     * @return array
     */
    private function formatResponseData(string $message): array {
        return [
            'message'=> $message
        ];
    }
}