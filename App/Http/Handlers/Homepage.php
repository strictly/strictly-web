<?php

namespace App\Http\Handlers;

use Strictly\Http\Response\ResponseFactory;
use Strictly\Http\Response\ResponseInterface;
use Strictly\Http\Routing\RequestHandlerInterface;

class Homepage implements RequestHandlerInterface
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * Homepage constructor.
     * @param ResponseFactory $responseFactory
     */
    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    /**
     * @return ResponseInterface
     */
    function handle(): ResponseInterface
    {
        return $this->responseFactory->make('Homepage :)');
    }
}