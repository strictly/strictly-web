<?php

use Strictly\Http\Routing\ManagerInterface;

/**
 * Register routes here.
 *
 * @var ManagerInterface $router
 */

$router->get('/', \App\Http\Handlers\Homepage::class);
