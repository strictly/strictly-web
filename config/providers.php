<?php

return [
    \App\ServiceProviders\ConsoleServiceProvider::class,
    \App\ServiceProviders\WebServiceProvider::class,
    \App\ServiceProviders\RoutingServiceProvider::class,
    \App\ServiceProviders\LogServiceProvider::class,
];