<?php

return [
    \Strictly\Console\Commands\ListCommands::class,
    \Strictly\Http\ConsoleCommands\Serve::class,
    \App\Console\Commands\HelloWorld::class,
];