<?php

return [
    'log' => [
        'repository' => env('LOG_ROUTING_REPOSITORY', false)
    ],
];