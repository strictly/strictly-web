<?php

$basePath = __DIR__.'/..';

require_once $basePath.'/vendor/autoload.php';

// Create the container repository.
$bindingRepository = new \Strictly\Container\ArrayRepository();

// Set container bindings.
$bindingRepository->setReference(\Strictly\Container\Repository::class, \Strictly\Container\ArrayRepository::class);
$bindingRepository->setInstance(\Strictly\Container\ArrayRepository::class, $bindingRepository);
$bindingRepository->setReference(Strictly\Container\ResolverInterface::class, \Strictly\Container\Resolver::class);
$bindingRepository->setReference(\Strictly\Container\Container::class, \Strictly\Container\PassthroughFacade::class);

// Set path binding.
$bindingRepository->setReference(\Strictly\Filesystem\Repository::class, \Strictly\Filesystem\BasePathRelativeRepository::class);
$bindingRepository->setInstance(\Strictly\Filesystem\BasePathRelativeRepository::class, new \Strictly\Filesystem\BasePathRelativeRepository($basePath));

// Set configuration bindings.
$bindingRepository->setReference(\Strictly\Configuration\Repository::class, \Strictly\Configuration\DotAdapter\DotAdapter::class);
$bindingRepository->setFactory(\Strictly\Configuration\DotAdapter\DotAdapter::class, \Strictly\Configuration\DotAdapter\SimpleDotAdapterFactory::class, true);
$bindingRepository->setReference(\Strictly\Foundation\ServiceProviderLoaderInterface::class, \Strictly\Foundation\ServiceProviderLoader::class);
$bindingRepository->setReference(\Strictly\Container\BindingLoader::class, \Strictly\Container\FileBindingLoader::class);
$bindingRepository->setFactory(\Strictly\Configuration\DotEnv\DotEnvLoader::class, \Strictly\Configuration\DotEnv\DotEnvLoaderFactory::class);
$bindingRepository->setFactory(\Strictly\Configuration\Loader::class, \App\Factories\ConfigurationLoaderFactory::class);
$bindingRepository->setReference(\Strictly\Foundation\Contracts\KernelInterface::class, \Strictly\Foundation\Kernel::class);

// return the resolver.
/** @var \Strictly\Container\ResolverInterface $container */
$container = new \Strictly\Container\Resolver($bindingRepository);